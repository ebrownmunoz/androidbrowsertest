package com.voxware.abrowser.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.voxware.abrowser.HelloAndroidActivity;
import com.voxware.abrowser.R;
import com.voxware.abrowser.R.id;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

public class FirstTest extends ActivityInstrumentationTestCase2<HelloAndroidActivity> {

    private HelloAndroidActivity mFirstTestActivity;
    private TextView mFirstTestText;
    
	public FirstTest() {
		super(HelloAndroidActivity.class);
	}
	
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mFirstTestActivity = getActivity();
        mFirstTestText =
                (TextView) mFirstTestActivity
                .findViewById(R.id.textView1);
    }

	@Test
	public void testOnStartCommandIntentIntInt() {
		fail("Not yet implemented " + mFirstTestText);
	}

}
